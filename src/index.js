// server.js : Web API Server entrypoint 
var apiServer = require('express-api-server');
 
var options = {
    baseUrlPath: '/api',
    cors: {
        "*": "*"
    },
};
 
var initRoutes = function(app, options) {
    // Set up routes off of base URL path 
    app.use(options.baseUrlPath, [
        require('./routes/todos.js')
    ]);
};
 
apiServer.start(initRoutes, options);