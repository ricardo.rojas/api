var express = require('express');
var errors = require('express-api-server').errors;
var jsonParser = require('body-parser').json();
 
var router = module.exports = express.Router();

router.route('/todos')
    .get(function(req, res, next) {
        var todos = [
            { 'name': 'Un todo', 'date' : new Date() },
            { 'name': 'Un todo', 'date' : new Date() },
            { 'name': 'Un todo', 'date' : new Date() }, 
        ]
        res.json(todos);
    })
    .post(jsonParser, function(req, res, next) {
        if (!req.body) { return next(new errors.BadRequestError()); }
        var newTodo = req.body
        res.status(200); 
        res.json(newTodo);
    });
